<?php

namespace App\Controllers;

class Produk extends BaseController
{
	var $produk_img_location = 'img/produk/' ;


	function __construct() {
		$this->session = session();
        $this->produk_model = new \App\Models\ProdukModel();

	}

	public function index()
	{
         $data = [
             'produk' => $this->produk_model->orderBy('id', 'desc')->paginate(10),
             'pager' => $this->produk_model->pager,
        ];

		return view('admin_produk/listing', $data );
	}

	function edit($id) {
		
		helper('form');
	 	$produk = $this->produk_model->find( $id );
		
		return view('admin_produk/edit', [
			'produk' => $produk ,
			'produk_img_location' => $this->produk_img_location
		]);
	}
//SLUG
	function slug($slug){
		$produk = $this->produk_model->where('slug',$slug)->first();
		return view('admin_produk/edit', [
			'produk' => $produk ,
			'produk_img_location' => $this->produk_img_location
		]);
	}

	function save_edit($id) {

		$data = [
			'nama' => $this->request->getPost('nama'),
			'keterangan' => $this->request->getPost('keterangan'),
            'harga' => $this->request->getPost('harga')
		];

		$file = $this->request->getFile('gambar');

		// Grab the file by name given in HTML form
		if ($file->isReadable())
		{		
			// Generate a new secure name
			$file_gambar = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move( $this->produk_img_location, $file_gambar);

			$data['gambar'] = $file_gambar;
		
		}
		$this->produk_model->update($id, $data);

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/produk/edit/'. $id);

	}


	function delete( $id ) {
		//$this->$produk_model = new \App\Models\ProdukModel();
		$this->produk_model->where('id', $id)->delete();

		$_SESSION['deleted'] = true;
		$this->session->markAsFlashdata('deleted');

		return redirect()->back();
	}

	function add() {		
		helper('form');
		return view('admin_produk/add');
	}

	// untuk save data dari add new form
	function save_new() {
		//$this->$produk_model = new \App\Models\ProdukModel();

		$data = [
			'nama' => $this->request->getPost('nama'),
			'keterangan' => $this->request->getPost('keterangan'),
			'harga' => $this->request->getPost('harga')
		];

		$file = $this->request->getFile('gambar');

		// Grab the file by name given in HTML form
		if ($file->isReadable())
		{		
			// Generate a new secure name
			$fail_gambar = $file->getRandomName();
		
			// Move the file to it's new home
			$file->move( $this->produk_img_location, $fail_gambar);

			$data['gambar'] = $fail_gambar;
		
		}

		$this->produk_model->insert( $data );

		$_SESSION['success'] = true;
		$this->session->markAsFlashdata('success');

		return redirect()->to('/produk');

		// echo "<h1>HELOO ... saya akan save data</h1>";
	}
}